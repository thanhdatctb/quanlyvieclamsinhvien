﻿use master
go 
DROP DATABASE IF EXISTS QuanLyViecLamSinhVien
GO
create database QuanLyViecLamSinhVien
go
use QuanLyViecLamSinhVien
go
create table tblSinhvien
(
	MaSV varchar(20) primary key,
	TenSv nvarchar(100)
)
go
create table tblNganhhoc
(
	MaNganh varchar(20) primary key,
	TenNganh nvarchar(100)
)
go
create table tblLinhvuc
(
	MaLinhVuc varchar(20) primary key,
	LinhVuc nvarchar(100),
	GhiChu nvarchar(1000)
)
go
create table tblVieclamSV
(
	id int primary key identity,
	MaSV varchar(20) foreign key references tblSinhvien(MaSV),
	MaNganh varchar(20) foreign key references tblNganhHoc(MaNganh),
	MaLinhVuc varchar(20) foreign key references tblLinhVuc(MaLinhVuc),
	NamRaTruong int,
	NgayVaoLam Date,
	NgayNghiLam Date,
	CongViecChinh nvarchar(100),
	LyDoNghiViec nvarchar(1000)
)
go 
insert into tblSinhVien values
('1234567', N'Nguyễn Việt Anh'),
('2345678', N'Nguyễn Trung Anh'),
('3456789', N'Nguyễn Văn Tân')
go
insert into tblNganhhoc values
('1234567',N'Quản trị kinh doanh'),
('2345678',N'Công nghệ thông tin'),
('3456789',N'Y đa khoa')
go
insert into tblLinhVuc values
('1234567',N'Sale Logistic',N'Cần tuyển 20 nhân viên'),
('2345678',N'Marketing',N'Sử dụng thành thạo photoshop, có 2 năm kinh nghiệm'),
('3456789',N'Quản trị mạng',N'Sử dụng thành thao Linux'),
('4567890',N'Quản trị kinh rủi ro',N'Chấp nhận sinh viên mới ra trường')
go
insert into tblVieclamSV (MaSV,Manganh,MaLinhVuc, NamRaTruong,NgayVaoLam, NgayNghiLam, CongViecChinh,LyDoNghiViec)values
('1234567','3456789','4567890',2018,'9/30/2018','5/30/2020',N'Duy trì hệ thống mạng công ty',N'Chuyển công tác'),
('2345678','2345678','3456789',2018,'9/30/2018','5/30/2020',N'Duy trì hệ thống mạng công ty',N'Chuyển công tác'),
('3456789','1234567','4567890',2018,'9/30/2018','5/30/2020',N'Duy trì hệ thống mạng công ty',N'Chuyển công tác')
--('1234567','3456789','4567890',2018,'9/30/2018','5/30/2020',N'Duy trì hệ thống mạng công ty',N'Chuyển công tác'),
--('1234567','3456789','4567890',2018,'9/30/2018','5/30/2020',N'Duy trì hệ thống mạng công ty',N'Chuyển công tác')
go 
select * from tblSinhvien