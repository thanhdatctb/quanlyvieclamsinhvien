namespace QuanLyViecLamSinhVien.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tblSinhvien")]
    public partial class tblSinhvien
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tblSinhvien()
        {
            tblVieclamSVs = new HashSet<tblVieclamSV>();
        }

        [Key]
        [StringLength(20)]
        public string MaSV { get; set; }

        [StringLength(100)]
        public string TenSv { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblVieclamSV> tblVieclamSVs { get; set; }
    }
}
