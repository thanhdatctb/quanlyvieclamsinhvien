namespace QuanLyViecLamSinhVien.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tblLinhvuc")]
    public partial class tblLinhvuc
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tblLinhvuc()
        {
            tblVieclamSVs = new HashSet<tblVieclamSV>();
        }

        [Key]
        [StringLength(20)]
        public string MaLinhVuc { get; set; }

        [StringLength(100)]
        public string LinhVuc { get; set; }

        [StringLength(1000)]
        public string GhiChu { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblVieclamSV> tblVieclamSVs { get; set; }
    }
}
