namespace QuanLyViecLamSinhVien.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tblVieclamSV")]
    public partial class tblVieclamSV
    {
        public int id { get; set; }

        [StringLength(20)]
        public string MaSV { get; set; }

        [StringLength(20)]
        public string MaNganh { get; set; }

        [StringLength(20)]
        public string MaLinhVuc { get; set; }

        public int? NamRaTruong { get; set; }

        [Column(TypeName = "date")]
        public DateTime? NgayVaoLam { get; set; }

        [Column(TypeName = "date")]
        public DateTime? NgayNghiLam { get; set; }

        [StringLength(100)]
        public string CongViecChinh { get; set; }

        [StringLength(1000)]
        public string LyDoNghiViec { get; set; }

        public virtual tblLinhvuc tblLinhvuc { get; set; }

        public virtual tblNganhhoc tblNganhhoc { get; set; }

        public virtual tblSinhvien tblSinhvien { get; set; }
    }
}
