namespace QuanLyViecLamSinhVien.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tblNganhhoc")]
    public partial class tblNganhhoc
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tblNganhhoc()
        {
            tblVieclamSVs = new HashSet<tblVieclamSV>();
        }

        [Key]
        [StringLength(20)]
        public string MaNganh { get; set; }

        [StringLength(100)]
        public string TenNganh { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblVieclamSV> tblVieclamSVs { get; set; }
    }
}
