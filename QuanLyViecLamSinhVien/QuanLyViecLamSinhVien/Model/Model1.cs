namespace QuanLyViecLamSinhVien.Model
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class Model1 : DbContext
    {
        public Model1()
            : base("name=Model1")
        {
        }

        public virtual DbSet<tblLinhvuc> tblLinhvucs { get; set; }
        public virtual DbSet<tblNganhhoc> tblNganhhocs { get; set; }
        public virtual DbSet<tblSinhvien> tblSinhviens { get; set; }
        public virtual DbSet<tblVieclamSV> tblVieclamSVs { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<tblLinhvuc>()
                .Property(e => e.MaLinhVuc)
                .IsUnicode(false);

            modelBuilder.Entity<tblNganhhoc>()
                .Property(e => e.MaNganh)
                .IsUnicode(false);

            modelBuilder.Entity<tblSinhvien>()
                .Property(e => e.MaSV)
                .IsUnicode(false);

            modelBuilder.Entity<tblVieclamSV>()
                .Property(e => e.MaSV)
                .IsUnicode(false);

            modelBuilder.Entity<tblVieclamSV>()
                .Property(e => e.MaNganh)
                .IsUnicode(false);

            modelBuilder.Entity<tblVieclamSV>()
                .Property(e => e.MaLinhVuc)
                .IsUnicode(false);
        }
    }
}
