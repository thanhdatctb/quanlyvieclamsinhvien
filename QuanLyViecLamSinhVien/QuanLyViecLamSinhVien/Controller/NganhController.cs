﻿using QuanLyViecLamSinhVien.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyViecLamSinhVien.Controller
{
    
    class NganhController
    {
        private Model1 db = new Model1();
        public List<tblNganhhoc> GetAll()
        {
            return db.tblNganhhocs.ToList();
        }
        public tblNganhhoc GetByMaNganh(String maNganh)
        {
            return db.tblNganhhocs.Where(s => s.MaNganh == maNganh).FirstOrDefault();
        }
        public bool Add(tblNganhhoc nganhhoc)
        {
            try
            {
                db.tblNganhhocs.Add(nganhhoc);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
        public bool Edit(tblNganhhoc nganhhoc)
        {
            try
            {
                //var editSinhvien = this.GetByMaSinhVien(sinhvien.MaSV);
                db.tblNganhhocs.AddOrUpdate(nganhhoc);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
        public Boolean Delete(String manganh)
        {
            try
            {
                var delete = this.GetByMaNganh(manganh);
                db.tblNganhhocs.Remove(delete);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
        public List<tblNganhhoc> Search(String keyword)
        {
            return db.tblNganhhocs.Where(s => s.MaNganh.Contains(keyword) || s.TenNganh.Contains(keyword)).ToList();
        }
    }
}
