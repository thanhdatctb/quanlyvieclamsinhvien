﻿using QuanLyViecLamSinhVien.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyViecLamSinhVien.Controller
{
    class ViecLamSVController
    {

        private Model1 db = new Model1();
        public List<tblVieclamSV> GetAll()
        {
            return db.tblVieclamSVs.ToList();
        }
        public tblVieclamSV GetByMaViecLam(int maViecLam)
        {
            return db.tblVieclamSVs.Where(s => s.id == maViecLam).FirstOrDefault();
        }
        public bool Add(tblVieclamSV vieclamsinhvien)
        {
            try
            {
                db.tblVieclamSVs.Add(vieclamsinhvien);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
        public bool Edit(tblVieclamSV vieclamsinhvien)
        {
            try
            {
                //var editSinhvien = this.GetByMaSinhVien(sinhvien.MaSV);
                db.tblVieclamSVs.AddOrUpdate(vieclamsinhvien);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
        public Boolean Delete(int id)
        {
            try
            {
                var delete = this.GetByMaViecLam(id);
                db.tblVieclamSVs.Remove(delete);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
        //public List<tblSinhvien> Search(String keyword)
        //{
        //    return db.tblVieclamSVs.Where(s => s.MaSV.Contains(keyword) || s.TenSv.Contains(keyword)).ToList();
        //}
    }
}
