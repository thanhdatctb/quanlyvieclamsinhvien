﻿using QuanLyViecLamSinhVien.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyViecLamSinhVien.Controller
{
    class LinhVucController
    {
        private Model1 db = new Model1();
        public List<tblLinhvuc> GetAll()
        {
            return db.tblLinhvucs.ToList();
        }
        public tblLinhvuc GetByMaLinhVuc(String maLinhVuc)
        {
            return db.tblLinhvucs.Where(s => s.MaLinhVuc == maLinhVuc).FirstOrDefault();
        }
        public bool Add(tblLinhvuc linhVuc)
        {
            try
            {
                db.tblLinhvucs.Add(linhVuc);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
        public bool Edit(tblLinhvuc linhVuc)
        {
            try
            {
                //var editSinhvien = this.GetByMaSinhVien(sinhvien.MaSV);
                db.tblLinhvucs.AddOrUpdate(linhVuc);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
        public Boolean Delete(String maLinhVuc)
        {
            try
            {
                var delete = this.GetByMaLinhVuc(maLinhVuc);
                db.tblLinhvucs.Remove(delete);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
        public List<tblLinhvuc> Search(String keyword)
        {
            return db.tblLinhvucs.Where(s => s.MaLinhVuc.Contains(keyword) || s.LinhVuc.Contains(keyword)|| s.GhiChu.Contains(keyword)).ToList();
        }

    }
}

