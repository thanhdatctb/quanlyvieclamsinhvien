﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using QuanLyViecLamSinhVien.Model;

namespace QuanLyViecLamSinhVien.Controller
{
    class SinhVienController
    {
        private Model1 db = new Model1();
        public List<tblSinhvien> GetAll()
        {
            return db.tblSinhviens.ToList();
        }
        public tblSinhvien GetByMaSinhVien(String masv)
        {
            return db.tblSinhviens.Where(s => s.MaSV == masv).FirstOrDefault();
        }
        public bool Add(tblSinhvien sinhvien)
        {
            try
            {
                db.tblSinhviens.Add(sinhvien);
                db.SaveChanges();
                return true;
            }
            catch(Exception ex) 
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
        public bool Edit(tblSinhvien sinhvien)
        {
            try
            {
                //var editSinhvien = this.GetByMaSinhVien(sinhvien.MaSV);
                db.tblSinhviens.AddOrUpdate(sinhvien);
                db.SaveChanges();
                return true;
            }catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
        public Boolean Delete(String maSv)
        {
            try
            {
                var delete = this.GetByMaSinhVien(maSv);
                db.tblSinhviens.Remove(delete);
                db.SaveChanges();
                return true;
            }catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
        public List<tblSinhvien> Search(String keyword)
        {
            return db.tblSinhviens.Where(s => s.MaSV.Contains(keyword) || s.TenSv.Contains(keyword)).ToList();
        }

    }
}
