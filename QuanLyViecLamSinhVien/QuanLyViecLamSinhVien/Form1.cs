﻿using QuanLyViecLamSinhVien.Controller;
using QuanLyViecLamSinhVien.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyViecLamSinhVien
{
    public partial class Form1 : Form
    {
        private SinhVienController sinhVienController = new SinhVienController();
        private NganhController nganhController = new NganhController();
        private LinhVucController linhVucController = new LinhVucController();
        private ViecLamSVController viecLamSVController = new ViecLamSVController();
        public Form1()
        {

            InitializeComponent();
            LoadData();
            AddBiding();


        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }
        private void AddBiding()
        {
            this.txtMaSinhVien.DataBindings.Add(new Binding("Text", dtgvSinhVien.DataSource, "MaSV", true, DataSourceUpdateMode.Never));
            this.txtHoVaTen.DataBindings.Add(new Binding("Text", dtgvSinhVien.DataSource, "TenSv", true, DataSourceUpdateMode.Never));

            this.txtManganh.DataBindings.Add(new Binding("Text", dtgvNganh.DataSource, "MaNganh", true, DataSourceUpdateMode.Never));
            this.txtTenNganh.DataBindings.Add(new Binding("Text", dtgvNganh.DataSource, "TenNganh", true, DataSourceUpdateMode.Never));

            this.txtMaLinhVuc.DataBindings.Add(new Binding("Text", dtgvLinhVuc.DataSource, "MaLinhVuc", true, DataSourceUpdateMode.Never));
            this.txtLinhVuc.DataBindings.Add(new Binding("Text", dtgvLinhVuc.DataSource, "LinhVuc", true, DataSourceUpdateMode.Never));
            this.txtGhiChu.DataBindings.Add(new Binding("Text", dtgvLinhVuc.DataSource, "GhiChu", true, DataSourceUpdateMode.Never));

            this.cbSinhVien.DataBindings.Add(new Binding("SelectedValue", dtgvViecLamSinhVien.DataSource, "MaSV", true, DataSourceUpdateMode.Never));
            this.cbNganh.DataBindings.Add(new Binding("SelectedValue", dtgvViecLamSinhVien.DataSource, "MaNganh", true, DataSourceUpdateMode.Never));
            this.cbLinhVuc.DataBindings.Add(new Binding("SelectedValue", dtgvViecLamSinhVien.DataSource, "MaLinhVuc", true, DataSourceUpdateMode.Never));
            this.nuNamRaTruong.DataBindings.Add(new Binding("Value", dtgvViecLamSinhVien.DataSource, "NamRaTruong", true, DataSourceUpdateMode.Never));
            this.dpNgayVaoLam.DataBindings.Add(new Binding("Value", dtgvViecLamSinhVien.DataSource, "NgayVaoLam", true, DataSourceUpdateMode.Never));
            this.dpNgayNghi.DataBindings.Add(new Binding("Value", dtgvViecLamSinhVien.DataSource, "NgayNghiLam", true, DataSourceUpdateMode.Never));
            this.txtCongViecChinh.DataBindings.Add(new Binding("Text", dtgvViecLamSinhVien.DataSource, "CongViecChinh", true, DataSourceUpdateMode.Never));
            this.txtLyDoNghi.DataBindings.Add(new Binding("Text", dtgvViecLamSinhVien.DataSource, "LyDoNghiViec", true, DataSourceUpdateMode.Never));
            this.txtId.DataBindings.Add(new Binding("Text", dtgvViecLamSinhVien.DataSource, "id", true, DataSourceUpdateMode.Never));

        }
        private void LoadData()
        {
            this.nuNamRaTruong.Maximum = DateTime.Now.Year;
            this.nuNamRaTruong.Value = DateTime.Now.Year;

            this.dtgvSinhVien.DataSource = sinhVienController.GetAll();
            this.dtgvNganh.DataSource = nganhController.GetAll();
            this.dtgvLinhVuc.DataSource = linhVucController.GetAll();
            this.dtgvViecLamSinhVien.DataSource = viecLamSVController.GetAll();

            this.cbSinhVien.DataSource = dtgvSinhVien.DataSource;
            this.cbSinhVien.ValueMember = "MaSV";
            this.cbSinhVien.DisplayMember = "TenSv";

            this.cbLinhVuc.DataSource = dtgvLinhVuc.DataSource;
            this.cbLinhVuc.ValueMember = "MaLinhVuc";
            this.cbLinhVuc.DisplayMember = "LinhVuc";

            this.cbNganh.DataSource = dtgvNganh.DataSource;
            this.cbNganh.ValueMember = "MaNganh";
            this.cbNganh.DisplayMember = "TenNganh";
        }

        private void btnThemSinhVien_Click(object sender, EventArgs e)
        {
            String maSv = this.txtMaSinhVien.Text;
            String HoVaten = this.txtHoVaTen.Text;
            tblSinhvien sinhvien = new tblSinhvien();
            sinhvien.MaSV = maSv;
            sinhvien.TenSv = HoVaten;
            if (sinhVienController.Add(sinhvien))
            {
                MessageBox.Show("Thêm sinh viên thành công");
                LoadData();
                this.Hide();
                new Form1().Show();
                
            }



        }

        private void button2_Click(object sender, EventArgs e)
        {
            String maSv = this.txtMaSinhVien.Text;
            String HoVaten = this.txtHoVaTen.Text;
            tblSinhvien sinhvien = new tblSinhvien();
            sinhvien.MaSV = maSv;
            sinhvien.TenSv = HoVaten;
            if (sinhVienController.Edit(sinhvien))
            {
                MessageBox.Show("Sửa sinh viên thành công");
                LoadData();
                this.Hide();
                new Form1().Show();

            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            String maSv = this.txtMaSinhVien.Text;
            
            if (sinhVienController.Delete(maSv))
            {
                MessageBox.Show("Xóa sinh viên thành công");
                LoadData();
                this.Hide();
                new Form1().Show();

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            String keyword = this.txtSearch.Text;
            this.dtgvSinhVien.DataSource = sinhVienController.Search(keyword);
        }

        private void button7_Click(object sender, EventArgs e)
        {
            String maNganh = this.txtManganh.Text;
            String tenNganh = this.txtTenNganh.Text;
            tblNganhhoc nganh = new tblNganhhoc();
            nganh.MaNganh = maNganh;
            nganh.TenNganh = tenNganh;
            if (nganhController.Add(nganh))
            {
                MessageBox.Show("Thêm ngành học thành công");
                LoadData();
                this.Hide();
                new Form1().Show();

            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            String maNganh = this.txtManganh.Text;
            String TenNganh = this.txtTenNganh.Text;
            tblNganhhoc nganh = new tblNganhhoc();
            nganh.MaNganh = maNganh;
            nganh.TenNganh = TenNganh;
            if (nganhController.Edit(nganh))
            {
                MessageBox.Show("Sửa ngành học thành công");
                LoadData();
                this.Hide();
                new Form1().Show();

            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            String maNganh = this.txtManganh.Text;

            if (nganhController.Delete(maNganh))
            {
                MessageBox.Show("Xóa ngành học thành công");
                LoadData();
                this.Hide();
                new Form1().Show();

            }
        }

        private void txtSearNganh_TextChanged(object sender, EventArgs e)
        {
            String keyword = this.txtSearNganh.Text;
            this.dtgvNganh.DataSource = nganhController.Search(keyword);
        }

        private void button11_Click(object sender, EventArgs e)
        {
            String maLinhVuc = this.txtMaLinhVuc.Text;
            String linhVuc = this.txtLinhVuc.Text;
            String GhiChu = this.txtGhiChu.Text;
            tblLinhvuc lv = new tblLinhvuc();
            lv.MaLinhVuc = maLinhVuc;
            lv.LinhVuc = linhVuc;
            lv.GhiChu = GhiChu;
            if (linhVucController.Add(lv))
            {
                MessageBox.Show("Thêm lĩnh vực thành công");
                LoadData();
                this.Hide();
                new Form1().Show();

            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            String maLinhVuc = this.txtMaLinhVuc.Text;
            String linhVuc = this.txtLinhVuc.Text;
            String GhiChu = this.txtGhiChu.Text;
            tblLinhvuc lv = new tblLinhvuc();
            lv.MaLinhVuc = maLinhVuc;
            lv.LinhVuc = linhVuc;
            lv.GhiChu = GhiChu;
            if (linhVucController.Edit(lv))
            {
                MessageBox.Show("Sửa lĩnh vực thành công");
                LoadData();
                this.Hide();
                new Form1().Show();

            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            String maLinhVuc = this.txtMaLinhVuc.Text;
            
            if (linhVucController.Delete(maLinhVuc))
            {
                MessageBox.Show("Xóa lĩnh vực thành công");
                LoadData();
                this.Hide();
                new Form1().Show();

            }
        }

        private void txtSearchLinhVuc_TextChanged(object sender, EventArgs e)
        {
            String keyword = this.txtSearchLinhVuc.Text;
            this.dtgvLinhVuc.DataSource = linhVucController.Search(keyword);
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button14_Click(object sender, EventArgs e)
        {
            int id = Int32.Parse(this.txtId.Text);
            String maSv = this.cbSinhVien.SelectedValue.ToString();
            String MaNganh = this.cbNganh.SelectedValue.ToString();
            String LinhVuc = this.cbLinhVuc.SelectedValue.ToString();
            int namRaTruong = Int32.Parse(this.nuNamRaTruong.Value.ToString());
            DateTime ngayVaoLam = this.dpNgayVaoLam.Value;
            DateTime ngayNghiViec = this.dpNgayNghi.Value;
            String CongViecChinh = this.txtCongViecChinh.Text;
            String LyDoNghiViec = this.txtLyDoNghi.Text;

            tblVieclamSV vieclamSV = new tblVieclamSV();
            vieclamSV.id = id;
            vieclamSV.MaSV = maSv;
            vieclamSV.MaNganh = MaNganh;
            vieclamSV.MaLinhVuc = LinhVuc;
            vieclamSV.NamRaTruong = namRaTruong;
            vieclamSV.NgayNghiLam = ngayNghiViec;
            vieclamSV.NgayVaoLam = ngayVaoLam;
            vieclamSV.CongViecChinh = CongViecChinh;
            vieclamSV.LyDoNghiViec = LyDoNghiViec;

            if (viecLamSVController.Add(vieclamSV))
            {
                MessageBox.Show("Thêm việc làm sinh viên thành công");
                LoadData();
                this.Hide();
                new Form1().Show();
            }
            
        }

        private void button13_Click(object sender, EventArgs e)
        {
            int id = Int32.Parse(this.txtId.Text);
            String maSv = this.cbSinhVien.SelectedValue.ToString();
            String MaNganh = this.cbNganh.SelectedValue.ToString();
            String LinhVuc = this.cbLinhVuc.SelectedValue.ToString();
            int namRaTruong = Int32.Parse(this.nuNamRaTruong.Value.ToString());
            DateTime ngayVaoLam = this.dpNgayVaoLam.Value;
            DateTime ngayNghiViec = this.dpNgayNghi.Value;
            String CongViecChinh = this.txtCongViecChinh.Text;
            String LyDoNghiViec = this.txtLyDoNghi.Text;

            tblVieclamSV vieclamSV = new tblVieclamSV();
            vieclamSV.id = id;
            vieclamSV.MaSV = maSv;
            vieclamSV.MaNganh = MaNganh;
            vieclamSV.MaLinhVuc = LinhVuc;
            vieclamSV.NamRaTruong = namRaTruong;
            vieclamSV.NgayNghiLam = ngayNghiViec;
            vieclamSV.NgayVaoLam = ngayVaoLam;
            vieclamSV.CongViecChinh = CongViecChinh;
            vieclamSV.LyDoNghiViec = LyDoNghiViec;

            if (viecLamSVController.Edit(vieclamSV))
            {
                MessageBox.Show("Sửa việc làm sinh viên thành công");
                LoadData();
                this.Hide();
                new Form1().Show();
            }
        }

        private void button12_Click(object sender, EventArgs e)
        {
            int id = Int32.Parse(this.txtId.Text);
            if (viecLamSVController.Delete(id))
            {
                MessageBox.Show("Xóa việc làm sinh viên thành công");
                LoadData();
                this.Hide();
                new Form1().Show();
            }
        }
    }
}
