se master
go 
DROP DATABASE IF EXISTS QuanLyViecLamSinhVien
GO
create database QuanLyViecLamSinhVien
go
use QuanLyViecLamSinhVien
go
USE [QuanLyViecLamSinhVien]
GO
/****** Object:  Table [dbo].[tblLinhvuc]    Script Date: 3/20/2021 3:33:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblLinhvuc](
	[MaLinhVuc] [varchar](20) NOT NULL,
	[LinhVuc] [nvarchar](100) NULL,
	[GhiChu] [nvarchar](1000) NULL,
PRIMARY KEY CLUSTERED 
(
	[MaLinhVuc] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblNganhhoc]    Script Date: 3/20/2021 3:33:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblNganhhoc](
	[MaNganh] [varchar](20) NOT NULL,
	[TenNganh] [nvarchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[MaNganh] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblSinhvien]    Script Date: 3/20/2021 3:33:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblSinhvien](
	[MaSV] [varchar](20) NOT NULL,
	[TenSv] [nvarchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[MaSV] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblVieclamSV]    Script Date: 3/20/2021 3:33:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblVieclamSV](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[MaSV] [varchar](20) NULL,
	[MaNganh] [varchar](20) NULL,
	[MaLinhVuc] [varchar](20) NULL,
	[NamRaTruong] [int] NULL,
	[NgayVaoLam] [date] NULL,
	[NgayNghiLam] [date] NULL,
	[CongViecChinh] [nvarchar](100) NULL,
	[LyDoNghiViec] [nvarchar](1000) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblVieclamSV]  WITH CHECK ADD FOREIGN KEY([MaLinhVuc])
REFERENCES [dbo].[tblLinhvuc] ([MaLinhVuc])
GO
ALTER TABLE [dbo].[tblVieclamSV]  WITH CHECK ADD FOREIGN KEY([MaNganh])
REFERENCES [dbo].[tblNganhhoc] ([MaNganh])
GO
ALTER TABLE [dbo].[tblVieclamSV]  WITH CHECK ADD FOREIGN KEY([MaSV])
REFERENCES [dbo].[tblSinhvien] ([MaSV])
GO
